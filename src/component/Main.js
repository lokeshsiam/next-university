import React from 'react';
import arrow from '../img/arrow.png';
import lo from '../img/lo.png';
import logo from '../img/logo.png';
import lock from '../img/lock.png';
import down from '../img/down.png';
import pen from '../img/pen.png';
import trash from '../img/trash.png';
import call from '../img/call.png';
import msg from '../img/msg.png';
import Navbar from './Navbar';
import "../component/Main.css";




function Main() {
   return (
      <>
         <div className='jet'>
            <Navbar />
            <div className="right">

               <div className="logo1">
                  <img src={logo} alt="" className="logo" />
               </div>

               <div className="top"><p><img src={arrow} alt="arrow"></img> Back</p></div>

               <div className="bottom">
                  <div className="header">
                     <div className="lo">
                        <img src={lo} alt="lo"></img>
                     </div>
                     <div className="text">
                        <h1>Malaysia College of Engineering</h1>
                        <p>No. 19 1A Jln Pandan 3/9 Taman Pandan Jaya</p>
                        <p>Malaysia</p>

                        <div className="lining"></div>
                        <p>Transforming dreams into reality with Malaysia.</p>
                        <div className="contact">
                           <div className="number"><img src={call} alt="call"></img> 672-434-4222</div></div>

                        <div className="mail"><img src={msg} alt="msg"></img> malaysiauniversity@ac.in</div>

                     </div>
                  </div >



                  <div className="overview">
                     <h3>Overview</h3>
                     <h6><img src={down} alt="down"></img> only me <img src={lock} alt="lock"></img></h6>


                     <div className="lining1"></div>
                     <p className='parah'>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniastrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis autere dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                     </p>
                     <div className="lining2"></div>
                     <div className="web">
                        <div className="website"><p>Website<br></br><a href="https://www.mu.club">https://www.mu.club</a></p></div>
                        <div className="website"><p>Industry<br></br>Education Management</p></div>
                        <div className="website"><p>Headquaters<br></br>Jaya,Malaysia</p></div>
                        <div className="website"><p><a href="change">change</a></p></div>
                     </div>
                     <br />
                     <div className="site">
                        <div className="website"><p>Company Size<br></br>201-500 employees</p></div>
                        <div className="website"><p>Founded<br></br>2017</p></div>
                        <div className="website"><p><br></br></p></div>
                        <div className="website"><p><br></br></p></div>
                     </div>
                  </div>


                  <div className="view">
                     <h3>Contact Person</h3>
                     <div className="line"></div>
                     <p>
                        Edit or add contact person for your organization changes will be sent to talent corp for approval.<a href="close">close</a>

                     </p>
                     <div className="line"></div>
                     <div className="person">
                        <div className="website"><p>Contact person<br></br></p></div>
                        <div className="website"><p>Job title<br></br></p></div>
                        <div className="website"><p>Date joined<br></br></p></div>

                     </div>
                     <div className="line"></div>
                     <div className="person">
                        <div className="website"><p>Alia Zulaikha <br></br>aliazulaikha@qlco.com </p></div>
                        <div className="website"><p>HR Manager<br></br></p></div>
                        <div className="website"><p>2 months<br></br></p></div>
                        <img src={pen} alt="pen" height="20px" ></img>
                        <img src={trash} alt="trash" height="20px" ></img>

                     </div>
                     <div className="line"></div>
                     <div className="person">
                        <div className="website"><p>Alia Zulaikha <br></br>aliazulaikha@qlco.com</p></div>
                        <div className="website"><p>HR Manager<br></br></p></div>
                        <div className="website"><p>2 months<br></br></p></div>
                        <img src={pen} alt="pen" height="20px" ></img>
                        <img src={trash} alt="trash" height="20px" ></img>

                     </div>
                     <div className="person">
                        <div className="website"><p><a href="https://www.mu.club">+ Add another contact person</a></p></div>
                     </div>
                  </div>
                  <div className="view">
                     <h3>Location</h3>
                     <h6><img src={down} alt="down"></img> only me <img src={lock} alt="lock"></img></h6>
                     <div className="line1"></div>
                     <div className="primary">
                        <p>primary<br></br>
                           <b>No. 19 1A Jln Pandan 3/9 Taman Jaya</b>< br></br>
                           <b>Malaysia 5600038, MA</b>  </p>
                           
                     </div>
                     <div className="line2"></div>
                     <div className="person1">
                        <div className="website"><p><a href="https://www.mu.club">+ Add secondary location</a></p></div>
                     </div>
                  </div>
               </div>
               <br></br>
               <div className="line3"></div>
               <div className="end">
                  <div className="locate-us">
                     <p>locate us :<br></br>
                        6th floor ,Surian Tower,1,Jalan PJU 7/3,Mutiara Damansara <br></br>
                        47810 Petaling Jaya, Selangor
                     </p>

                  </div>
                  <div className="contact-us">
                     <p>contact us :<br></br>
                        +603 7839 7000</p>

                  </div>

               </div>
            </div>

         </div>
      </>
   );
}

export default Main;