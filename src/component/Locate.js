import React from "react";
import '../component/Locate.css';


function Locate() {
    return (
        <>
            <div className="underline"></div>

            <div className="locate">
                <h3>Locate Us</h3>
                <h3>6th floor, surian Tower, 1, jalan PJU7/3 Mutiara Damansara,</h3>
                <h3>47810 petaling jaya, selangor</h3>
            </div>

            <div className="contact">
                <h3>Contact Us:</h3>
                <h2>+603 7839 7000</h2>
                
            </div>
        </>
    );
}


export default Locate;