import React from 'react';
import mynext from '../img/mynext.png';
import Home from '../img/Home.png';
import Hat from '../img/Hat.png';
import Man from '../img/Man.png';
import set from '../img/set.png';
import help from '../img/help.png';
import { Link } from "react-router-dom";
import "../component/Navbar.css";

function Navbar() {
    return (
        <>
            <div className="side">

                <div className="sidebar">
                    
                        <div className="University">

                            <h3 className="home">
                                <img src={mynext} alt="next" height="50px" />

                                <p>University</p>
                            </h3>
                        </div>
                    

                    <Link to={`/`} className='links'>
                        <div className="hu" >
                            <div className="bar">
                                <img src={Home} alt="home" height="20px" />
                            </div>
                            <h3 className="home">
                                Dashboard
                            </h3>
                            
                        </div>
                    </Link>
                    <div className="bar">

                        <div className="bar">
                            <img src={Man} alt="home" height="20px" />
                        </div>
                        <h3 className="home">
                            Students
                        </h3>
                    </div>
                    
                    <div className="bar">
                   
                        <div className="bar">
                            <img src={Hat} alt="home" height="20px" />
                        </div>
                        <Link to={`/second`} className='links'> 
                        <h3 className="home">
                            My University
                        </h3>
                        </Link>
                    </div>
                  
                    <div className="bar">

                        <div className="bar">
                            <img src={set} alt="home" height="20px" />
                        </div>
                        <h3 className="home">
                            Settings
                        </h3>
                    </div>

                    <div className="bar">

                        <div className="bar">
                            <img src={help} alt="home" height="20px" />
                        </div>
                        <h3 className="home">
                            Help
                        </h3>
                    </div>

                </div>
            </div>
        </>
    )
}

export default Navbar;