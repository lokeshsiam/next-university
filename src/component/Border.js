import React from "react";
import './Border.css';
import circle from '../img/circle.png';
import man2 from '../img/man2.png';
import dot from '../img/dot.png';
import Card from '../img/Card.png';
import '../component/Locate'
import Locate from "../component/Locate";

function Border() {
    return (
        <>
            <div className="border1">
                <p className="head1">Assessment result on average</p>
                <img src={circle} alt="" className="circle" />

                <ul className="list">
                    <li >ideas and oppurtunities</li>
                    <li>Resource</li>
                    <li>Into action</li>
                </ul>

                <div>
                    <img src={man2} alt="boy" className="man2" />
                </div>
            </div>

            <div className="border2">
                <div className="">

                    <h1 className="cb">Comparative Bar</h1>
                    <img src={dot} alt="" className="dot" />
                </div>

                <div>
                    <p className="numb1">43% EnglishProficiency 57%</p>
                    <label for="file"></label>
                    <progress id="file" value="43" max="100" className="bar1"></progress>
                    <p className="planned">planned</p>
                    <p className="Notplanned">Not planned</p>
                </div>

                <div>
                    <p className="numb2">19% ITSkills 81%</p>
                    <label for="file"></label>
                    <progress id="file" value="19" max="100" className="bar2"></progress>
                    <p className="documented">Documented</p>
                    <p className="notdocumented">Not Documented</p>
                </div>

                <div>
                    <p className="numb3">68% EmployementFactor 32%</p>
                    <label for="file"></label>
                    <progress id="file" value="68" max="100" className="bar3"></progress>
                    <p className="Designed">Designed</p>
                    <p className="notDesigned">Not Designed</p>
                </div>

                <div>
                    <p className="numb4">94% OtherFactor 6%</p>
                    <label for="file"></label>
                    <progress id="file" value="94" max="100"className="bar4"></progress>
                    <p className="coded">coded</p>
                    <p className="notcoded">Not coded</p>
                </div>
            </div>

            <div className="border3">
                <img src={Card} alt="" className="Card" />
            </div>
<Locate/>
        </>
    );
}


export default Border;