import React from "react";
import '../component/Sidebar.css';
import logo from '../img/logo.png';
import Navbar  from "../component/Navbar";
import Border from "../component/Border"


function Sidebar() {
    return (
        <>
          <Navbar/>
            <div className="outer">
                <div className="texts">
                    <p>Good Morning</p>
                </div>
                <div className="logo">
                    <img src={logo} alt="" className="logo" />
                </div>
            </div>

            <div className="box1">
                <h1 className="num1">12800</h1>
                <p className="text1">Registred Students</p>
            </div>

            <div className="box2">
                <h1 className="num2">28</h1>
                <p className="text2">Counselling Scheduled</p>
            </div>

            <div className="box3">
                <h1 className="num3">920</h1>
                <p className="text3">Internship Offered</p>
            </div>

           <Border/>

        </>
    );
}

export default Sidebar;