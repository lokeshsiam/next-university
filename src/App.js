
import Sidebar from './component/Sidebar';
import {BrowserRouter as Router,Switch,Route } from "react-router-dom"
import Main from './component/Main';
import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
      
        <Switch>
          <Route exact path="/" component={Sidebar}></Route>
          <Route exact path="/second" component={Main}></Route>
        </Switch>
    </Router>
     
   
    </div>
  );
}

export default App;
